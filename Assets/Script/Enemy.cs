﻿using UnityEngine;
using System.Collections;

public class Enemy : MonoBehaviour {
	public float speed = 5f;
	public int points = 100;
    Rigidbody2D rb;

	void Start(){
		rb = GetComponent<Rigidbody2D> ();
		rb.velocity = new Vector2 (0, -speed);
	}

	void Update () {
		if (transform.position.y < -5) {
			Destroy (gameObject);
		}
	}
}