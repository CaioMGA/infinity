﻿using UnityEngine;
using System.Collections;

using UnityEngine.UI;
using UnityEngine.EventSystems;
using UnityEngine.Events;

public class GameLogic : MonoBehaviour {
	public float scrollSpeed = 0.2f;
    private GameObject faixas;
    private GameObject canteiro;
    public Text scoreText;
	public Text highscoreText;
	public GameObject[] enemies;
	public Canvas replayGUI;
	private bool gameover = false;
    public int score;
    private int highscore;

    public int scoreIncrement = 1;

    private bool isCenarioMoving = false;
    // Use this for initialization
    void Start () {
		Time.timeScale = 1f;
        faixas = GameObject.FindWithTag("Faixas");
        canteiro = GameObject.FindWithTag("Canteiro");
        InvokeRepeating ("sendEnemy", 1f, 2f);
        score = 0;
        highscore = PlayerPrefs.GetInt ("highscore");
		highscoreText.text = "Highscore: " + highscore;
        InvokeRepeating("playerScores", 0.1f, 0.1f);
        InvokeRepeating("speedUp", 0.1f, 5f);

    }
	
	// Update is called once per frame
	void Update () {
		if (!gameover) {
            loopFaixas();
            loopCanteiro();
            atualizaScore();
		} else {
			CancelInvoke("sendEnemy");
            CancelInvoke("playerScores");
            CancelInvoke("speedUp");
            replayGUI.enabled = true;
			if(score > highscore){
				PlayerPrefs.SetInt ("highscore", score);
			}
		}

	}

    void speedUp() {
        Time.timeScale *= 1.1f;
        Debug.Log("Speed Up");
    }

    void loopFaixas() {
        int totalFaixas = faixas.transform.childCount;
        for (int i = 0; i < totalFaixas; i++) {
            Transform faixa = faixas.transform.GetChild(i);
            faixa.position = new Vector2(faixa.position.x, faixa.position.y - scrollSpeed);
            if (faixa.position.y <= -8.8f) {
                faixa.position = new Vector2(faixa.position.x, 5.2f);
            }
        }
    }

    void loopCanteiro() {
        int totalPedacos = canteiro.transform.childCount;
        for (int i = 0; i < totalPedacos; i++) {
            Transform pedaco = canteiro.transform.GetChild(i);
            pedaco.position = new Vector2(pedaco.position.x, pedaco.position.y - scrollSpeed);
            if (pedaco.position.y < -19f) {
                pedaco.position = new Vector2(pedaco.position.x, 14f);
            }
        }
    }

    void atualizaScore(){
		scoreText.text = "Score: "+ score;
	}

	void sendEnemy(){
		int maxEnemyType = enemies.Length;
		int enemyType = Random.Range (0, maxEnemyType);
		GameObject newEnemy = Instantiate (enemies [enemyType]);
		int position = Random.Range (-2, 3);
		newEnemy.transform.position = new Vector2 (position, newEnemy.transform.position.y);

		enemyType = Random.Range (0, maxEnemyType);
		GameObject otherEnemy = Instantiate (enemies [enemyType]);
		int otherPosition = position;
		while(position == otherPosition){
			otherPosition = Random.Range (-2, 3);
		}
		otherEnemy.transform.position = new Vector2 (otherPosition, otherEnemy.transform.position.y);

		newEnemy.transform.parent = transform;
		otherEnemy.transform.parent = transform;
	}

	public void gameOver(){
		gameover = true;
		Debug.Log ("GAME OVER");
	}

    void playerScores() {
        score += scoreIncrement;
    }

    public void dangerDodge() {
        score += 20;
        Debug.Log("DANGER ZONE!");
        //toca musica
        //mostra alguma coisa na tela
    }
}
