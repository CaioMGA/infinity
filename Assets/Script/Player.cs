﻿using UnityEngine;
using System.Collections;

public class Player : MonoBehaviour {

    private GameLogic gameLogic;

    void Awake() {
        gameLogic = GameObject.FindWithTag("GameLogic").GetComponent< GameLogic>();
    }

	void Update () {
		getInput ();
	}

	void getInput(){
		if (Input.GetKeyDown(KeyCode.A) || Input.GetKeyDown (KeyCode.LeftArrow)) {
			if(transform.position.x > -2){
				transform.position = new Vector2(transform.position.x -1, transform.position.y);
			}
		} else if (Input.GetKeyDown(KeyCode.D) || Input.GetKeyDown(KeyCode.RightArrow)) {
			if(transform.position.x < 2){
				transform.position = new Vector2(transform.position.x +1, transform.position.y);
			}
		}
	}
	void OnTriggerEnter2D(Collider2D other){

        if (other.gameObject.CompareTag("Enemy")) {
            transform.GetComponentInParent<GameLogic>().gameOver();

			Destroy (gameObject);
		}
	}

    void OnTriggerExit2D(Collider2D other) {
        if (other.gameObject.CompareTag("DangerZone")) {
            gameLogic.dangerDodge();
            //
        }
    }
}
