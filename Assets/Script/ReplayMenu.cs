﻿using UnityEngine;
using System.Collections;

using UnityEngine.UI;
using UnityEngine.EventSystems;
using UnityEngine.Events;

public class ReplayMenu : MonoBehaviour {

	public Text scoreText;
	private bool atualizouText;
	private Canvas menuCanvas;
    private GameLogic gameLogic;

    void Awake(){
		atualizouText = false;
		menuCanvas = GetComponent<Canvas> ();
        gameLogic = GameObject.FindGameObjectWithTag("GameLogic").GetComponent<GameLogic>();
	}

	void getText(){
		if (!atualizouText && menuCanvas.enabled) {
			scoreText.text = "Score: " + gameLogic.score;
			atualizouText = true;
		}
	}

	void Update(){
		getText ();
	}

	public void restart(){
		Application.LoadLevel (Application.loadedLevelName);
	}

	public void quit(){
		Application.Quit ();
	}
}